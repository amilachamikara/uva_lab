/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.List;
import model.Hib;
import model.MainStock;
import model.Product;
import model.Users;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Amila
 */
public class TestDB {
    public static void main(String[] args) {
        
        Session session = Hib.start();
        
        //Product p = new Product(0, "Test Tube", "200ml", "GlassWare", "Chemistry");
        Product p = (Product)session.get(Product.class, 0);
        
        MainStock ms = (MainStock)session.load(MainStock.class, p.getProductId());
        
        System.out.println(ms.getStock());
        
        Hib.stop();
    }
}
