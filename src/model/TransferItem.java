/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Amila
 */
@Entity
@Table(name = "TransferItem")
public class TransferItem implements Serializable{
    @Id @GeneratedValue (strategy = GenerationType.AUTO)
    private int transferItemId;
    @OneToOne
    private Product product;
    @Column(name = "quantity")
    private double quantity;
    @Column(name = "itemNo")
    private int itemNo;

    public TransferItem() {
    }

    public TransferItem(Product product, double quantity, int itemNo) {
        this.product = product;
        this.quantity = quantity;
        this.itemNo = itemNo;
    }

    public int getTransferItemId() {
        return transferItemId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public int getItemNo() {
        return itemNo;
    }

    public void setItemNo(int itemNo) {
        this.itemNo = itemNo;
    }
    
    
    
    
}
