/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Amila
 */
@Entity
@Table(name = "ChemStock")
public class ChemStock extends AbstractStock implements Serializable {
    
    @Id 
    @OneToOne
    @JoinColumn
    private Product product;
    @Column(name = "stock")
    private double stock;
    

    public ChemStock() {
    }
    
    

    public ChemStock(Product product, double stock) {
        this.product = product;
        this.stock = stock;
    }
    
    
    

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getStock() {
        return stock;
    }

    public void setStock(double stock) {
        this.stock = stock;
    }
}
