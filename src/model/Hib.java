package model;

import java.util.logging.Level;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Amila
 */
public class Hib {

    private static Session session;
    private static SessionFactory factory;

    public static Session start() {
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.OFF);

        Configuration c = new Configuration();
        factory = c.configure().buildSessionFactory();
        session = factory.openSession();

        session.beginTransaction();
        return session;

    }

    public static void stop() {
        if (session.isOpen()) {
            session.getTransaction().commit();
            session.close();
            factory.close();
        }
    }
}
