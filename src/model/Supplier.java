/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Amila
 */
@Entity
@Table(name="Supplier")
public class Supplier {
    @Id
    private int supplierId;
    @Column(name = "name")
    private String name;
    @Column(name = "company")
    private String company;
    @Column(name = "contactNo")
    private String contactNo;

    public Supplier() {
    }

    public Supplier(int supplierId, String name, String company, String contactNo) {
        this.supplierId = supplierId;
        this.name = name;
        this.company = company;
        this.contactNo = contactNo;
    }

    

    
    
    
    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
