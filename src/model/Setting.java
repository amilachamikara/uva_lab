/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;

/**
 *
 * @author Amila
 */
public class Setting implements Serializable {

    private boolean hasProduct;
    private boolean hasSupplier;
    private boolean hasMainStock;
    private boolean hasPurchases;
    private boolean hasTransfers;
    private boolean hasReports;
    private boolean hasLabStock;

    public Setting(boolean hasProduct, boolean hasSupplier, boolean hasMainStock, boolean hasPurchases, boolean hasTransfers, boolean hasReports, boolean hasLabStock) {
        this.hasProduct = hasProduct;
        this.hasSupplier = hasSupplier;
        this.hasMainStock = hasMainStock;
        this.hasPurchases = hasPurchases;
        this.hasTransfers = hasTransfers;
        this.hasReports = hasReports;
        this.hasLabStock = hasLabStock;
    }

    public boolean isHasProduct() {
        return hasProduct;
    }

    public void setHasProduct(boolean hasProduct) {
        this.hasProduct = hasProduct;
    }

    public boolean isHasSupplier() {
        return hasSupplier;
    }

    public void setHasSupplier(boolean hasSupplier) {
        this.hasSupplier = hasSupplier;
    }

    public boolean isHasMainStock() {
        return hasMainStock;
    }

    public void setHasMainStock(boolean hasMainStock) {
        this.hasMainStock = hasMainStock;
    }

    public boolean isHasPurchases() {
        return hasPurchases;
    }

    public void setHasPurchases(boolean hasPurchases) {
        this.hasPurchases = hasPurchases;
    }

    public boolean isHasTransfers() {
        return hasTransfers;
    }

    public void setHasTransfers(boolean hasTransfers) {
        this.hasTransfers = hasTransfers;
    }

    public boolean isHasReports() {
        return hasReports;
    }

    public void setHasReports(boolean hasReports) {
        this.hasReports = hasReports;
    }

    public boolean isHasLabStock() {
        return hasLabStock;
    }

    public void setHasLabStock(boolean hasLabStock) {
        this.hasLabStock = hasLabStock;
    }

}
