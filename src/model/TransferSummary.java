/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Amila
 */
@Entity
@Table(name = "TransferSummary")
public class TransferSummary implements Serializable {
    @Id @GeneratedValue (strategy = GenerationType.AUTO)
    private int transferId;
    @Column (name = "lab")
    private String lab;
    @Column (name ="date")
    private Date date;
    @Column (name = "noOfItem")
    private double noOfItems;
    @OneToMany (fetch = FetchType.EAGER)
    private Collection<TransferItem> transferItems;

    public TransferSummary() {
    }
    
    public TransferSummary(String lab, Date date, double noOfItems, Collection<TransferItem> transferItems) {
        this.lab = lab;
        this.date = date;
        this.noOfItems = noOfItems;
        this.transferItems = transferItems;
    }

    public int getTransferId() {
        return transferId;
    }

    public String getLab() {
        return lab;
    }

    public void setLab(String lab) {
        this.lab = lab;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public double getNoOfItems() {
        return noOfItems;
    }

    public void setNoOfItems(double noOfItems) {
        this.noOfItems = noOfItems;
    }

    public Collection<TransferItem> getTransferItems() {
        return transferItems;
    }

    public void setTransferItems(Collection<TransferItem> transferItems) {
        this.transferItems = transferItems;
    }
    
    
    
    
}
