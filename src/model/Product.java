/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Amila
 */
@Entity
@Table(name = "product")
public class Product implements Serializable {
    @Id 
    private int productId;
    @Column(name = "description")
    private String description;
    @Column(name = "specification")
    private String Specification;
    @Column(name = "type")
    private String type;
    @Column(name = "lab")
    private String lab;

    public Product() {
    }
    
    
    
    public Product(int productId, String description, String specification, String type, String lab){
        this.productId = productId;
        this.description = description;
        this.Specification = specification;
        this.type = type;
        this.lab = lab;
    }

    /**
     * @return the productId
     */
    public int getProductId() {
        return productId;
    }

    /**
     * @param productId the productId to set
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the Specification
     */
    public String getSpecification() {
        return Specification;
    }

    /**
     * @param Specification the Specification to set
     */
    public void setSpecification(String Specification) {
        this.Specification = Specification;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the lab
     */
    public String getLab() {
        return lab;
    }

    /**
     * @param lab the lab to set
     */
    public void setLab(String lab) {
        this.lab = lab;
    }
}
