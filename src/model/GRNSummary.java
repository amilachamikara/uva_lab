/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Amila
 */
@Entity
@Table(name = "GRNSummary")
public class GRNSummary {
    @Id @GeneratedValue
    private int grnId;
    @OneToOne
    private Supplier supplier;
    @OneToMany (fetch = FetchType.EAGER)
    private Collection<GRNItem> grnItems;
    @Column(name = "date")
    private Date date;
    @Column(name = "subtotal")
    private double subtotal;
    @Column(name = "discount")
    private double discount;
    @Column(name = "total")
    private double total;

    public GRNSummary() {
    }

    public GRNSummary(Supplier supplier, Collection<GRNItem> grnItems, Date date, double subtotal, double discount, double total) {
        this.supplier = supplier;
        this.grnItems = grnItems;
        this.date = date;
        this.subtotal = subtotal;
        this.discount = discount;
        this.total = total;
    }

    
    
    public int getGrnId() {
        return grnId;
    }

    public void setGrnId(int grnId) {
        this.grnId = grnId;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Collection<GRNItem> getGrnItems() {
        return grnItems;
    }

    public void setGrnItems(Collection<GRNItem> grnItems) {
        this.grnItems = grnItems;
    }
    
}
