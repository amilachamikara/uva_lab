/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Amila
 */
@Entity
@Table(name="users")
public class Users implements Serializable {
    @Id @GeneratedValue (strategy = GenerationType.AUTO)
    private int userid;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "type")
    private String type;

    public Users() {
    }

    
    public Users(String username, String password, String type) {
        this.username = username;
        this.password = password;
        this.type = type;
    }

    
    
    public int getUserid() {
        return userid;
    }

    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    
}
