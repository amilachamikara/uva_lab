
package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Amila
 */
@Entity
@Table(name = "GRNItem")
public class GRNItem {
    @Id @GeneratedValue
    private int grnItemId;
    @OneToOne
    private Product product;
    @Column(name = "unitPrice")
    private double unitPrice;
    @Column(name = "quantity")
    private double quantity;
    @Column(name = "value")
    private double value;
    @Column(name = "itemNo")
    private int itemNo;

    public GRNItem() {
    }

    public GRNItem(Product product, double unitPrice, double quantity, double value, int itemNo) {
        this.product = product;
        this.unitPrice = unitPrice;
        this.quantity = quantity;
        this.value = value;
        this.itemNo = itemNo;
    }
    
    

    public int getGrnItemId() {
        return grnItemId;
    }

    public void setGrnItemId(int grnItemId) {
        this.grnItemId = grnItemId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getItemNo() {
        return itemNo;
    }

    public void setItemNo(int itemNo) {
        this.itemNo = itemNo;
    }
}
