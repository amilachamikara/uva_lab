/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import model.Hib;
import model.Users;
import org.apache.derby.drda.NetworkServerControl;

/**
 *
 * @author Amila
 */
public class LoginController implements Initializable {

    @FXML
    private Button btn_login;
    @FXML
    private Button btn_cancel;
    @FXML
    private TextField txt_username;
    @FXML
    private PasswordField txt_password;

    @FXML
    private void btn_loginAction(ActionEvent event) throws Exception {
       
        boolean isValidUser = false;
        List<Users> list = Hib.start().createQuery("FROM Users").list();
        Hib.stop();
        for (Users u : list) {
            if (u.getUsername().equals(txt_username.getText()) && u.getPassword().equals(txt_password.getText())) {
                isValidUser = true;
                MyApplication.setCurrentUser(u);
                break;
            }
        }

        if (isValidUser) {
            Parent root = FXMLLoader.load(getClass().getResource("../view/MainMenu.fxml"));
            MyApplication.getStage().setScene(new Scene(root));
            MyApplication.getStage().centerOnScreen();
            MyApplication.getStage().setResizable(false);
        } else {
            new Alert(Alert.AlertType.ERROR, "Username or Password is incorrect.\nTry again!", ButtonType.OK).show();
            txt_username.setText(null);
            txt_password.setText(null);
            txt_username.requestFocus();
        }

    }

    @FXML
    private void btn_cancelAction(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Do you want to exit?", ButtonType.YES, ButtonType.NO);
        if (alert.showAndWait().get().equals(ButtonType.YES)) {
            MyApplication.getStage().close();
        }
    }
    
    
    @FXML
    private void lnk_signUpAction(ActionEvent event)throws Exception {
            Parent root = FXMLLoader.load(getClass().getResource("../view/SignUp.fxml"));
            Stage stage = new Stage();
            
            stage.setScene(new Scene(root));
            stage.centerOnScreen();
            stage.setResizable(false);
            //stage.setAlwaysOnTop(true);
            stage.setTitle("Sign Up");
            stage.show();
    }
    
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        txt_username.setOnKeyPressed(e->{if(e.getCode().equals(KeyCode.ENTER))txt_password.requestFocus();});
//        txt_password.setOnKeyPressed(e->{if(e.getCode().equals(KeyCode.ENTER))btn_login.fire();});
    }

}
