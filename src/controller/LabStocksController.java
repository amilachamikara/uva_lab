/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import model.AbstractStock;
import model.Hib;

/**
 * FXML Controller class
 *
 * @author Amila
 */
public class LabStocksController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private ComboBox<String> cmb_lab;
    
    //table
    @FXML
    private TableView<AbstractStock> tbl_labstock;
    //table columns
    @FXML
    private TableColumn<AbstractStock, String> description;
    @FXML
    private TableColumn<AbstractStock, Double> stock;
    
    private ObservableList<AbstractStock> table_data = FXCollections.observableArrayList();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        stock.setCellValueFactory(new PropertyValueFactory<AbstractStock, Double>("stock"));
        description.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<AbstractStock, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<AbstractStock, String> param) {
                return new SimpleObjectProperty<>(param.getValue().getProduct().getDescription());
            }
        });
        
        tbl_labstock.setItems(table_data);
        loadLabs();
        
        cmb_lab.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                loadStock(newValue);
            }

            private void loadStock(String newValue) {
                table_data.clear();
                if(newValue.equals("Biology")){
                    List<AbstractStock> list = Hib.start().createCriteria(model.BioStock.class).list();
                    table_data.addAll(list);
                    Hib.stop();
                }else if(newValue.equals("Chemistry")){
                    List<AbstractStock> list = Hib.start().createCriteria(model.ChemStock.class).list();
                    table_data.addAll(list);
                    Hib.stop();
                }else if(newValue.equals("Physics")){
                    List<AbstractStock> list = Hib.start().createCriteria(model.PhyStock.class).list();
                    table_data.addAll(list);
                    Hib.stop();
                }
            }
        });
        
    }    

    private void loadLabs() {
        List<String> list = new ArrayList<String>();
        list.add("Biology");
        list.add("Chemistry");
        list.add("Physics");
        cmb_lab.getItems().setAll(list);
    }
    
}
