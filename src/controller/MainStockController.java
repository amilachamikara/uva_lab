/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import model.Hib;
import model.MainStock;

/**
 * FXML Controller class
 *
 * @author Amila
 */
public class MainStockController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TitledPane productPane;
    @FXML
    private TextField txt_pid;
    @FXML
    private TextField txt_desc;
    @FXML
    private TextArea txt_spec;
    @FXML
    private TextField txt_type;
    @FXML
    private TextField txt_lab;
    
    
    //table
    @FXML
    private TableView<MainStock> tbl_mainstock;
    //table columns
    @FXML
    private TableColumn<MainStock, Double> stock;
    @FXML
    private TableColumn<MainStock, String> productName;
    
    
    ObservableList<MainStock> table_data = FXCollections.observableArrayList();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        stock.setCellValueFactory(new PropertyValueFactory<MainStock, Double>("stock"));
        productName.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<MainStock, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<MainStock, String> param) {
                return new SimpleObjectProperty<>(param.getValue().getProduct().getDescription());
            }
        });
        tbl_mainstock.setItems(table_data);
        loadMainStock();
        
        tbl_mainstock.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<MainStock>() {

            @Override
            public void changed(ObservableValue<? extends MainStock> observable, MainStock oldValue, MainStock newValue) {
                txt_pid.setText(String.valueOf(newValue.getProduct().getProductId()));
                txt_desc.setText(newValue.getProduct().getDescription());
                txt_spec.setText(newValue.getProduct().getSpecification());
                txt_type.setText(newValue.getProduct().getType());
                txt_lab.setText(newValue.getProduct().getLab());
                productPane.setExpanded(true);
            }
        });
        
    }    

    private void loadMainStock() {
        List<MainStock> list = Hib.start().createQuery("FROM MainStock").list();
        table_data.clear();
        table_data.addAll(list);
        Hib.stop();
    }
    
}
