/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import model.Setting;

/**
 * FXML Controller class
 *
 * @author Amila
 */
public class MainMenuController implements Initializable {

    @FXML
    private Button btn_products;
    @FXML
    private Button btn_suppliers;
    @FXML
    private Button btn_mainstock;
    @FXML
    private Button btn_purchase;
    @FXML
    private Button btn_transfers;
    @FXML
    private Button btn_reports;
    @FXML
    private Button btn_labstocks;
    @FXML
    private Button btn_settings;

    @FXML
    private void btn_productsAction(ActionEvent event) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../view/Products.fxml"));
//        MyApplication.getStage().setScene(new Scene(root));
//        MyApplication.getStage().centerOnScreen();
//        MyApplication.getStage().setResizable(false);
        Scene scene = new Scene(root);

        Stage stage = new Stage();
        stage.setTitle("Products");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    private void btn_purchaseAction(ActionEvent event) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../view/Purchase.fxml"));
        Scene scene = new Scene(root);

        Stage stage = new Stage();
        stage.setTitle("Purchase");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    private void btn_mainStockAction(ActionEvent event) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../view/MainStock.fxml"));
        Scene scene = new Scene(root);

        Stage stage = new Stage();
        stage.setTitle("Main Stock");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    private void btn_supplierAction(ActionEvent event) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../view/Supplier.fxml"));
        Scene scene = new Scene(root);

        Stage stage = new Stage();
        stage.setTitle("Supplier");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    private void btn_transfersAction(ActionEvent event) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../view/Transfers.fxml"));
        Scene scene = new Scene(root);

        Stage stage = new Stage();
        stage.setTitle("Transfers");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    private void btn_labStocksAction(ActionEvent event) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../view/LabStocks.fxml"));
        Scene scene = new Scene(root);

        Stage stage = new Stage();
        stage.setTitle("Lab Stocks");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    private void btn_reportsAction(ActionEvent event) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../view/Reports.fxml"));
        Scene scene = new Scene(root);

        Stage stage = new Stage();
        stage.setTitle("Reports");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    private void btn_settingsAction(ActionEvent event) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("../view/Settings.fxml"));
        Scene scene = new Scene(root);

        Stage stage = new Stage();
        stage.setTitle("Settings");
        stage.setResizable(false);
        stage.setScene(scene);

        stage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        if (MyApplication.getCurrentUser().getType().equals("User")) {

            try {
                FileInputStream fileIn = new FileInputStream("src/priv.txt");
                ObjectInputStream in = new ObjectInputStream(fileIn);
                Setting s = (Setting) in.readObject();
                in.close();
                fileIn.close();

                btn_products.setDisable(!s.isHasProduct());
                btn_suppliers.setDisable(!s.isHasSupplier());
                btn_purchase.setDisable(!s.isHasPurchases());
                btn_transfers.setDisable(!s.isHasTransfers());
                btn_reports.setDisable(!s.isHasReports());
                btn_mainstock.setDisable(!s.isHasMainStock());
                btn_labstocks.setDisable(!s.isHasLabStock());
                btn_settings.setDisable(true);
            } catch (Exception e) {
                System.out.println(e);
            }

        }
    }

}
