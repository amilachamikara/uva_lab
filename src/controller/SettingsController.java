/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TitledPane;
import javafx.stage.Stage;
import model.Setting;

/**
 * FXML Controller class
 *
 * @author Amila
 */
public class SettingsController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private CheckBox chk_products;
    @FXML
    private CheckBox chk_suppliers;
    @FXML
    private CheckBox chk_purchases;
    @FXML
    private CheckBox chk_transfers;
    @FXML
    private CheckBox chk_reports;
    @FXML
    private CheckBox chk_mainstock;
    @FXML
    private CheckBox chk_labstock;
    @FXML
    private TitledPane privPane;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            FileInputStream fileIn = new FileInputStream("src/priv.txt");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Setting s = (Setting) in.readObject();
            in.close();
            fileIn.close();

            chk_products.setSelected(s.isHasProduct());
            chk_suppliers.setSelected(s.isHasSupplier());
            chk_purchases.setSelected(s.isHasPurchases());
            chk_transfers.setSelected(s.isHasTransfers());
            chk_reports.setSelected(s.isHasReports());
            chk_mainstock.setSelected(s.isHasMainStock());
            chk_labstock.setSelected(s.isHasLabStock());

        } catch (Exception e) {
            System.out.println(e);
        }
        
        
        privPane.requestFocus();
        privPane.setExpanded(true);
    }

    
    
    @FXML
    private void btn_saveAction(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Do you want to save these settings?", ButtonType.YES, ButtonType.NO);
        if (alert.showAndWait().get().equals(ButtonType.YES)) {

            try {
                FileOutputStream fileOut = new FileOutputStream("src/priv.txt");
                ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
                objOut.writeObject(new Setting(
                        chk_products.isSelected(),
                        chk_suppliers.isSelected(),
                        chk_mainstock.isSelected(),
                        chk_purchases.isSelected(),
                        chk_transfers.isSelected(),
                        chk_reports.isSelected(),
                        chk_labstock.isSelected()));
                objOut.close();
                fileOut.close();
            } catch (Exception e) {
                System.out.println(e);
            }

            ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();

        }

    }

    @FXML
    private void btn_cancelAction(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }

}
