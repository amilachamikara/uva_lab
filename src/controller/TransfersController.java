/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import model.BioStock;
import model.ChemStock;
import model.GRNItem;
import model.Hib;
import model.MainStock;
import model.PhyStock;
import model.Product;
import model.TransferItem;
import model.TransferSummary;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author Amila
 */
public class TransfersController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private ComboBox<Product> cmb_product;
    @FXML
    private ComboBox<String> cmb_lab;
    @FXML
    private TextField txt_stock;
    @FXML
    private TextField txt_qty;

    @FXML
    private DatePicker datePicker;
    @FXML
    private TextField noOfItems;

    //table
    @FXML
    private TableView tbl_transfers;
    //table columns
    @FXML
    private TableColumn<TransferItem, Integer> itemNo;
    @FXML
    private TableColumn<TransferItem, String> description;
    @FXML
    private TableColumn<TransferItem, Double> qty;

    private ObservableList<TransferItem> table_data = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        itemNo.setCellValueFactory(new PropertyValueFactory<TransferItem, Integer>("itemNo"));
        description.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TransferItem, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<TransferItem, String> param) {
                return new SimpleObjectProperty<>(param.getValue().getProduct().getDescription());
            }
        });
        qty.setCellValueFactory(new PropertyValueFactory<TransferItem, Double>("quantity"));

        loadProducts();
        loadLabs();
        tbl_transfers.setItems(table_data);

        cmb_product.setCellFactory(new Callback<ListView<Product>, ListCell<Product>>() {

            @Override
            public ListCell<Product> call(ListView<Product> param) {
                return new ListCell<Product>() {

                    @Override
                    protected void updateItem(Product item, boolean empty) {
                        super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getDescription());
                        }
                    }

                };
            }
        });

        cmb_product.setConverter(new StringConverter<Product>() {

            @Override
            public String toString(Product object) {
                if (object == null) {
                    return null;
                } else {
                    return object.getDescription();
                }
            }

            @Override
            public Product fromString(String string) {
                ObservableList<Product> list = cmb_product.getItems();
                Product prd = null;
                for (Product p : list) {
                    if (p.getDescription().equals(string)) {
                        prd = p;
                    }
                }
                return prd;

            }
        });

        table_data.addListener(new ListChangeListener<TransferItem>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends TransferItem> c) {
                noOfItems.setText(String.valueOf(table_data.size()));
            }
        });

        tbl_transfers.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.BACK_SPACE || event.getCode() == KeyCode.DELETE) {

                    table_data.remove(tbl_transfers.getSelectionModel().getSelectedItem());
                    numberItems();
                }
            }
        });

        cmb_product.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Product>() {

            @Override
            public void changed(ObservableValue<? extends Product> observable, Product oldValue, Product newValue) {
                MainStock ms = (MainStock) Hib.start().load(model.MainStock.class, newValue.getProductId());
                txt_stock.setText(String.valueOf(ms.getStock()));
                Hib.stop();
            }
        });

        datePicker.setValue(LocalDate.now());

    }

    @FXML
    private void btn_addItemAction(ActionEvent event) {

        try {
            if (Double.valueOf(txt_qty.getText()) > Double.valueOf(txt_stock.getText())) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Available Stock");
                alert.setContentText("Available stock is not sufficient!");
                alert.showAndWait();
                txt_qty.selectAll();
                txt_qty.requestFocus();
            } else {

                if (isInList(cmb_product.getValue())) {
                    Alert alert = new Alert(AlertType.ERROR);
                    alert.setHeaderText(null);
                    alert.setTitle("Item");
                    alert.setContentText("This item is already in the list! Add another one!");
                    alert.showAndWait();
                    clearFields();
                } else {

                    Product p = cmb_product.getValue();
                    double q = txt_qty.getText().isEmpty() ? 0.0 : Double.valueOf(txt_qty.getText());
                    TransferItem tItem = new TransferItem(p, q, 0);
                    table_data.add(tItem);
                    numberItems();
                    clearFields();
                }
            }
        } catch (NumberFormatException e) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Invalid Input");
            alert.setContentText("Input format of quantity is invalid!");
            alert.showAndWait();
            txt_qty.selectAll();
            txt_qty.requestFocus();

        }
    }

    @FXML
    private void btn_clearAllAction(ActionEvent event) {
    }

    @FXML
    private void btn_transferAction(ActionEvent event) {
        if (cmb_lab.getSelectionModel().getSelectedIndex() == -1) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setTitle("Required Field");
            alert.setContentText("Please select any lab to transfer items!");
            alert.show();
        } else if (table_data.size() == 0) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setTitle("No Items added");
            alert.setContentText("Please add items to transfer!");
            alert.show();
        } else {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setHeaderText(null);
            alert.setTitle("Transfer");
            alert.setContentText("Do you want to transfer these items?");

            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.OK) {

                updateStock();

            }
        }

    }

    private void loadProducts() {
        Query query = Hib.start().createQuery("FROM Product");
        cmb_product.getItems().setAll(query.list());
        Hib.stop();
    }

    private void numberItems() {
        ObservableList<TransferItem> list = table_data;
        int i = 1;
        for (TransferItem t : list) {
            t.setItemNo(i++);
        }
    }

    private void loadLabs() {
        List<String> list = new ArrayList<String>();
        list.add("Biology");
        list.add("Chemistry");
        list.add("Physics");
        cmb_lab.getItems().setAll(list);
    }

    private void updateStock() {
        Session s = Hib.start();
        Collection<TransferItem> list = table_data;

        for (TransferItem t : list) {
            s.saveOrUpdate(t);
            MainStock ms = (MainStock) s.load(model.MainStock.class, t.getProduct().getProductId());
            ms.setStock(ms.getStock() - t.getQuantity());
            s.saveOrUpdate(ms);

            if (cmb_lab.getValue().equals("Biology")) {
                BioStock bs = (BioStock) s.load(model.BioStock.class, t.getProduct().getProductId());
                bs.setStock(bs.getStock() + t.getQuantity());
                s.saveOrUpdate(bs);
            } else if (cmb_lab.getValue().equals("Physics")) {
                PhyStock ps = (PhyStock) s.load(model.PhyStock.class, t.getProduct().getProductId());
                ps.setStock(ps.getStock() + t.getQuantity());
                s.saveOrUpdate(ps);
            } else if (cmb_lab.getValue().equals("Chemistry")) {
                ChemStock cs = (ChemStock) s.load(model.ChemStock.class, t.getProduct().getProductId());
                cs.setStock(cs.getStock() + t.getQuantity());
                s.saveOrUpdate(cs);
            }

        }

        TransferSummary summary = new TransferSummary(cmb_lab.getValue(), Date.valueOf(datePicker.getValue()), Double.valueOf(noOfItems.getText()), list);
        s.saveOrUpdate(summary);

        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Trans");
        alert.setContentText("Items have been transfered to '" + cmb_lab.getValue() + "' lab successfully!");
        alert.show();
        Hib.stop();
    }

    private void clearFields() {
        txt_stock.setText(null);
        txt_qty.setText("0");
        cmb_product.show();
    }

    private boolean isInList(Product p) {
        boolean b = false;
        for(TransferItem i: table_data){
            if(i.getProduct().equals(p)){
                b = true;
                break;
            }
        }
        
        return b;
    }
}
