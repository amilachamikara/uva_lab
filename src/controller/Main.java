/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.derby.drda.NetworkServerControl;

/**
 *
 * @author Amila
 */
public class Main extends Application {
    
    
    @Override
    public void start(Stage stage) throws Exception {
        
        Parent root = FXMLLoader.load(getClass().getResource("../view/Login.fxml"));
        Scene scene = new Scene(root);
        
        MyApplication.setScene(scene);
        MyApplication.setStage(stage);
        MyApplication.setServerThread(new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    NetworkServerControl server = new NetworkServerControl();
                    server.start(null);
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        }));
        MyApplication.getServerThread().start();
        stage.setScene(scene);
        stage.setTitle("User Login");
        stage.show();
        
    }

    @Override
    public void stop() throws Exception {
        MyApplication.getServerThread().stop();
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * @return the scene
     */
    
    
}
