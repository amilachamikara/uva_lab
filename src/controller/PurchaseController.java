/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import model.GRNItem;
import model.GRNSummary;
import model.Hib;
import model.MainStock;
import model.Product;
import model.Supplier;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author Amila
 */
public class PurchaseController implements Initializable {

    @FXML
    private ComboBox<Product> cmb_search;
    @FXML
    private ComboBox<Supplier> cmb_supplier;

    @FXML
    private Button btn_addItem;
    @FXML
    private TextField txt_qty;
    @FXML
    private TextField txt_unit;
    @FXML
    private Label lbl_subtotal;
    @FXML
    private Label lbl_total;
    @FXML
    private TextField txt_discount;
    @FXML
    private DatePicker datePicker;
    @FXML
    private TitledPane grnDetails;
    @FXML
    private TitledPane summary;
    @FXML
    private Label lbl_path;
    @FXML
    private CheckBox chk_save;
    

    // table
    @FXML
    private TableView<GRNItem> tbl_grn;

    // table_columns
    @FXML
    private TableColumn<GRNItem, Integer> itemNo;
    @FXML
    private TableColumn<GRNItem, String> description;
    @FXML
    private TableColumn<GRNItem, Double> qty;
    @FXML
    private TableColumn<GRNItem, Double> unitPrice;
    @FXML
    private TableColumn<GRNItem, Double> value;

    //private ObservableList<String> search_data = FXCollections.observableArrayList();
    private ObservableList<GRNItem> table_data = FXCollections.observableArrayList();
    private File savePath = new File("purchase_note.pdf");

    @FXML
    private void btn_addItemAction(ActionEvent event) {

        if (cmb_search.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setTitle("Required Input");
            alert.setContentText("No product has been selected!");
            alert.showAndWait();
            cmb_search.show();
            cmb_search.requestFocus();
        } else {
            try {
                double u = txt_unit.getText().isEmpty() ? 0.0 : Double.valueOf(txt_unit.getText());
                double q = txt_qty.getText().isEmpty() ? 0.0 : Double.valueOf(txt_qty.getText());
                double v = u * q;
                Product p = cmb_search.getValue();
                GRNItem grni = new GRNItem(p, u, q, v, 0);
                table_data.add(grni);
                numberItems();
            } catch (NumberFormatException e) {
                Alert alert = new Alert(AlertType.ERROR);
                alert.setHeaderText(null);
                alert.setTitle("Invalid Input");
                alert.setContentText("Input format of quantity or unit price is invalid!\nCheck you inputs again!");
                alert.showAndWait();
            }
        }
    }

    @FXML
    private void cmb_searchOnKeyTyped(ActionEvent event) {
        cmb_search.show();
    }

    @FXML
    private void btn_cancelAction(ActionEvent event) {
        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
    }
    
    
    @FXML
    private void btn_browseAction(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Save As");
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Portable Document Format", "*.pdf"));
        File f = chooser.showSaveDialog(((Button)event.getSource()).getScene().getWindow());
        savePath = f!=null?f:savePath;
        lbl_path.setText(savePath.getName());
    }
    
    
    @FXML
    private void btn_purchaseAction(ActionEvent event) {
        if (cmb_supplier.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setTitle("Required Input");
            alert.setContentText("No supplier has been selected!");
            
            grnDetails.setExpanded(true);
            alert.showAndWait();
            cmb_supplier.requestFocus();
            cmb_supplier.show();
                
        } else {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setHeaderText(null);
            alert.setTitle("Purchase");
            alert.setContentText("Do you want to purchase these items?");

            Optional<ButtonType> result = alert.showAndWait();

            if (result.get() == ButtonType.OK) {
                updateStock();
                if(chk_save.isSelected()){
                    saveToFile(savePath);
                }
            }
            
            
        }

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        itemNo.setCellValueFactory(new PropertyValueFactory<GRNItem, Integer>("itemNo"));
        description.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GRNItem, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<GRNItem, String> param) {
                return new SimpleObjectProperty<>(param.getValue().getProduct().getDescription());
            }
        });
        qty.setCellValueFactory(new PropertyValueFactory<GRNItem, Double>("quantity"));
        unitPrice.setCellValueFactory(new PropertyValueFactory<GRNItem, Double>("unitPrice"));
        value.setCellValueFactory(new PropertyValueFactory<GRNItem, Double>("value"));

        //cmb_search.setItems(search_data);
        cmb_search.setVisibleRowCount(10);
        tbl_grn.setItems(table_data);
        cmb_search.setCellFactory(new Callback<ListView<Product>, ListCell<Product>>() {

            @Override
            public ListCell<Product> call(ListView<Product> param) {
                return new ListCell<Product>() {

                    @Override
                    protected void updateItem(Product item, boolean empty) {
                        super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getDescription());
                        }
                    }

                };
            }
        });

        cmb_search.setConverter(new StringConverter<Product>() {

            @Override
            public String toString(Product object) {
                if (object == null) {
                    return null;
                } else {
                    return object.getDescription();
                }
            }

            @Override
            public Product fromString(String string) {
                ObservableList<Product> list = cmb_search.getItems();
                Product prd = null;
                for (Product p : list) {
                    if (p.getDescription().equals(string)) {
                        prd = p;
                    }
                }
                return prd;

            }
        });

        loadProducts();

        cmb_search.getEditor().setOnKeyReleased(e -> {
//            loadProducts(cmb_search.getEditor().getText());
//            System.out.println(cmb_search.getEditor().getText());
            cmb_search.show();
        });

        //cmb_supplier
        cmb_supplier.setCellFactory(new Callback<ListView<Supplier>, ListCell<Supplier>>() {

            @Override
            public ListCell<Supplier> call(ListView<Supplier> param) {
                return new ListCell<Supplier>() {

                    @Override
                    protected void updateItem(Supplier item, boolean empty) {
                        super.updateItem(item, empty); //To change body of generated methods, choose Tools | Templates.
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getName());
                        }
                    }

                };
            }
        });

        cmb_supplier.setConverter(new StringConverter<Supplier>() {

            @Override
            public String toString(Supplier object) {
                if (object == null) {
                    return null;
                } else {
                    return object.getName();
                }
            }

            @Override
            public Supplier fromString(String string) {
                ObservableList<Supplier> list = cmb_supplier.getItems();
                Supplier splr = null;
                for (Supplier s : list) {
                    if (s.getName().equals(string)) {
                        splr = s;
                    }
                }
                return splr;
            }
        });

        loadSuppliers();

        table_data.addListener(new ListChangeListener<GRNItem>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends GRNItem> c) {
                ObservableList<? extends GRNItem> list = c.getList();
                double subtotal = 0.0;
                double total = 0.0;
                double discount = txt_discount.getText().isEmpty() ? 0.0 : Double.valueOf(txt_discount.getText());
                for (GRNItem g : list) {
                    subtotal += g.getValue();
                }
                total = subtotal - discount;
                lbl_subtotal.setText(String.valueOf(subtotal));
                lbl_total.setText(String.valueOf(total));
            }
        });

        txt_discount.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                double subtotal = Double.valueOf(lbl_subtotal.getText());
                double discount = txt_discount.getText().isEmpty() ? 0.0 : Double.valueOf(txt_discount.getText());
                double total = subtotal - discount;
                lbl_total.setText(String.valueOf(total));
            }
        });

        tbl_grn.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.BACK_SPACE || event.getCode() == KeyCode.DELETE) {

                    table_data.remove(tbl_grn.getSelectionModel().getSelectedItem());
                    numberItems();
                }
            }
        });
        
        datePicker.setValue(LocalDate.now());

    }

    private void loadProducts() {
        Query query = Hib.start().createQuery("FROM Product");

        cmb_search.getItems().setAll(query.list());

        Hib.stop();
    }

    private void loadSuppliers() {
        Query query = Hib.start().createQuery("FROM Supplier");

        cmb_supplier.getItems().setAll(query.list());

        Hib.stop();
    }

    private void updateStock() {
        Session s = Hib.start();
        Collection<GRNItem> list = table_data;
        double stock = 0.0;
        for (GRNItem i : list) {
            s.saveOrUpdate(i);
            MainStock ms = (MainStock) s.load(model.MainStock.class, i.getProduct().getProductId());
            ms.setStock(ms.getStock() + i.getQuantity());
            s.saveOrUpdate(ms);
        }

        GRNSummary summary = new GRNSummary(cmb_supplier.getValue(), list, Date.valueOf(datePicker.getValue()), Double.valueOf(lbl_subtotal.getText()), Double.valueOf(txt_discount.getText()), Double.valueOf(lbl_total.getText()));

        s.saveOrUpdate(summary);

        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setTitle("Info");
        alert.setContentText("Main stock has been updated!");
        alert.show();

        Hib.stop();

    }

    private void numberItems() {
        ObservableList<GRNItem> list = table_data;
        int i = 1;
        for (GRNItem g : list) {
            g.setItemNo(i++);
        }
    }

    private void saveToFile(File savePath) {
        Document document = new Document(PageSize.A4);
        try {
            PdfWriter.getInstance(document, new FileOutputStream(savePath));
            document.open();
            
            Paragraph title = new Paragraph("Goods Received Note");
            title.setAlignment(Paragraph.ALIGN_CENTER);
            //title.setFont(Font.BOLD);
            title.setSpacingAfter(10f);
            title.setSpacingAfter(10f);
            document.add(title);
            
            Paragraph para = new Paragraph("Laboratory Management System\nUva Wellassa University");
            para.setAlignment(Paragraph.ALIGN_CENTER);
            para.setSpacingAfter(20f);
            document.add(para);
            
            Paragraph sup = new Paragraph("Supplier: " + cmb_supplier.getValue().getName());
            sup.setAlignment(Paragraph.ALIGN_LEFT);
            sup.setSpacingAfter(10f);
            document.add(sup);
            Paragraph date = new Paragraph("Date: " + datePicker.getValue().format(DateTimeFormatter.ISO_DATE));
            date.setAlignment(Paragraph.ALIGN_LEFT);
            date.setSpacingAfter(20f);
            document.add(date);
            
            PdfPTable table = new PdfPTable(5);
            table.addCell("Item No");
            table.addCell("Description");
            table.addCell("Unit Price");
            table.addCell("Qty");
            table.addCell("Value");
            
            for (GRNItem i: table_data) {
                table.addCell(String.valueOf(i.getItemNo()));
                table.addCell(i.getProduct().getDescription());
                table.addCell(String.valueOf(i.getUnitPrice()));
                table.addCell(String.valueOf(i.getQuantity()));
                table.addCell(String.valueOf(i.getValue()));
            }
            
            PdfPCell details = new PdfPCell();
            details.addElement(new Paragraph("No of Items: " + table_data.size()));
            details.addElement(new Paragraph("Sub Total: " + lbl_subtotal.getText()));
            details.addElement(new Paragraph("Discount: " + txt_discount.getText()));
            details.addElement(new Paragraph("Totla: " + lbl_total.getText()));
            details.setColspan(5);
            table.addCell(details);
            
            document.add(table);
            
            document.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
