/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Users;

/**
 *
 * @author Amila
 */
public class MyApplication {
    
    private static Stage stage;
    private static Scene scene;
    private static Users currentUser;
    private static Thread serverThread;

    /**
     * @return the stage
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * @param aStage the stage to set
     */
    public static void setStage(Stage aStage) {
        stage = aStage;
    }

    /**
     * @return the scene
     */
    public static Scene getScene() {
        return scene;
    }

    /**
     * @param aScene the scene to set
     */
    public static void setScene(Scene aScene) {
        scene = aScene;
    }

    public static Users getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(Users aCurrentUser) {
        currentUser = aCurrentUser;
    }

    public static Thread getServerThread() {
        return serverThread;
    }

    public static void setServerThread(Thread aServerThread) {
        serverThread = aServerThread;
    }

    

    
}
