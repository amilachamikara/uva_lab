/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Hib;
import model.Setting;
import model.Users;
import org.hibernate.criterion.Restrictions;

/**
 * FXML Controller class
 *
 * @author Amila
 */
public class SignUpController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TextField txt_username;
    @FXML
    private PasswordField txt_password;
    @FXML
    private PasswordField txt_confPassword;
    @FXML
    private ComboBox<String> cmb_accType;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loadTypes();

    }

    @FXML
    private void btn_cancelAction(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Do you want to cancel?", ButtonType.YES, ButtonType.NO);
        if (alert.showAndWait().get().equals(ButtonType.YES)) {
            ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();

        }
    }

    @FXML
    private void btn_createAction(ActionEvent event) {
        if (!cmb_accType.getSelectionModel().isEmpty()) {
            if (cmb_accType.getSelectionModel().getSelectedItem().equals("Administrator")) {
                List<model.Users> list = Hib.start().createCriteria(model.Users.class).add(Restrictions.eq("type", "Administrator")).list();
                Hib.stop();
                if (!list.isEmpty()) {
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setHeaderText(null);
                    alert.setTitle("Account Type");
                    alert.setContentText("One administrator is already exists!");
                    alert.show();
                    cmb_accType.getSelectionModel().clearSelection();

                } else {
                    if (isValid()) {
                        String un = txt_username.getText();
                        String pw = txt_password.getText();
                        String type = cmb_accType.getValue();

                        Hib.start().save(new Users(un, pw, type));
                        Hib.stop();
                        try {
                            FileOutputStream fileOut = new FileOutputStream("src/priv.txt");
                            ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
                            objOut.writeObject(new Setting(false, false, false, false, false, false, true));
                            objOut.close();
                            fileOut.close();
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setHeaderText(null);
                        alert.setTitle("Successful");
                        alert.setContentText("New user has been created!");
                        alert.showAndWait();
                        ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
                    }
                }
            } else {
                if (isValid()) {
                    String un = txt_username.getText();
                    String pw = txt_password.getText();
                    String type = cmb_accType.getValue();

                    Hib.start().save(new Users(un, pw, type));
                    Hib.stop();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setHeaderText(null);
                    alert.setTitle("Successful");
                    alert.setContentText("New user has been created!");
                    alert.showAndWait();
                    ((Stage) ((Button) event.getSource()).getScene().getWindow()).close();
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setHeaderText(null);
            alert.setTitle("Account Type");
            alert.setContentText("Please select any account type!");
            alert.show();
        }
    }

    private void loadTypes() {
        ObservableList<String> list = FXCollections.observableArrayList();
        list.add("Administrator");
        list.add("User");
        cmb_accType.setItems(list);
    }

    private boolean isValid() {
        boolean b = false;
        if (!txt_password.getText().equals(txt_confPassword.getText())) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Inputs");
            alert.setContentText("Password and conformation password are not matched!");
            alert.showAndWait();
            b = false;
        } else {
            b = true;
        }

        if (txt_username.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Inputs");
            alert.setContentText("One or more fields are empty!");
            alert.showAndWait();
            b = false;
        } else {
            b = true;
        }

        return b;
    }
}
