/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import model.GRNItem;
import model.GRNSummary;
import model.Hib;
import model.TransferItem;
import model.TransferSummary;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;

/**
 * FXML Controller class
 *
 * @author Amila
 */
public class ReportsController implements Initializable {

    /**
     * Initializes the controller class.
     */
    //table grnSummary
    @FXML
    private TableView<GRNSummary> tbl_grnsummary;
    @FXML
    private TableColumn<GRNSummary, Integer> grnId;
    @FXML
    private TableColumn<GRNSummary, Date> grnDate;
    @FXML
    private TableColumn<GRNSummary, String> supplier;
    
    //table grn
    @FXML
    private TableView<GRNItem> tbl_grn;
    @FXML
    private TableColumn<GRNItem, Integer> itemNo;
    @FXML
    private TableColumn<GRNItem, String> description;
    @FXML
    private TableColumn<GRNItem, Double> qty;
    @FXML
    private TableColumn<GRNItem, Double> unitPrice;
    @FXML
    private TableColumn<GRNItem, Double> value;
    
    //table transferSummary
    @FXML
    private TableView<TransferSummary> tbl_transferSummary;
    @FXML 
    private TableColumn<TransferSummary, Integer> transferId;
    @FXML
    private TableColumn<TransferSummary, Date> transferDate;
    @FXML
    private TableColumn<TransferSummary, String> lab;
    
    
    //table transfer
    @FXML
    private TableView<TransferItem> tbl_transfers;
    @FXML
    private TableColumn<TransferItem, Integer> transItemNo;
    @FXML
    private TableColumn<TransferItem, String> transDescription;
    @FXML
    private TableColumn<TransferItem, Double> transQty;
    
    
    //grn details
    @FXML
    private TextField txt_grnId;
    @FXML
    private TextField txt_grnDate;
    @FXML
    private TextField txt_supplier;
    @FXML
    private TextField txt_subtotal;
    @FXML
    private TextField txt_discount;
    @FXML
    private TextField txt_total;
    @FXML
    private TitledPane grnDetails;
    
    //transfer details
    @FXML
    private TextField txt_transferId;
    @FXML
    private TextField txt_transferDate;
    @FXML
    private TextField txt_recLab;
    @FXML
    private TextField txt_noOfItems;
    @FXML
    private TitledPane transferDetails;
    
    
    
    private ObservableList<GRNSummary> grntable_data = FXCollections.observableArrayList();
    private ObservableList<GRNItem> grnItemtable_data = FXCollections.observableArrayList();
    private ObservableList<TransferSummary> transfertable_data = FXCollections.observableArrayList();
    private ObservableList<TransferItem> transferItemtable_data = FXCollections.observableArrayList();
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
        //grn 
        
        grnId.setCellValueFactory(new PropertyValueFactory<GRNSummary, Integer>("grnId"));
        grnDate.setCellValueFactory(new PropertyValueFactory<GRNSummary, Date>("date"));
        supplier.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GRNSummary, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<GRNSummary, String> param) {
                return new SimpleObjectProperty<>(param.getValue().getSupplier().getName());
            }
        });
        tbl_grnsummary.setItems(grntable_data);
        
        
        itemNo.setCellValueFactory(new PropertyValueFactory<GRNItem, Integer>("itemNo"));
        description.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<GRNItem, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<GRNItem, String> param) {
                return new SimpleObjectProperty<>(param.getValue().getProduct().getDescription());
            }
        });
        qty.setCellValueFactory(new PropertyValueFactory<GRNItem, Double>("quantity"));
        unitPrice.setCellValueFactory(new PropertyValueFactory<GRNItem, Double>("unitPrice"));
        value.setCellValueFactory(new PropertyValueFactory<GRNItem, Double>("value"));
        tbl_grn.setItems(grnItemtable_data);
        loadGRNSummaries();
        
        
        tbl_grnsummary.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<GRNSummary>() {

            @Override
            public void changed(ObservableValue<? extends GRNSummary> observable, GRNSummary oldValue, GRNSummary newValue) {
                txt_grnId.setText(String.valueOf(newValue.getGrnId()));
                txt_grnDate.setText(String.valueOf(newValue.getDate()));
                txt_supplier.setText(newValue.getSupplier().getName());
                txt_subtotal.setText(String.valueOf(newValue.getSubtotal()));
                txt_discount.setText(String.valueOf(newValue.getDiscount()));
                txt_total.setText(String.valueOf(newValue.getTotal()));
                
                grnDetails.setExpanded(true);
                Collection<GRNItem> list = new ArrayList<>();
                list.addAll(newValue.getGrnItems());
                grnItemtable_data.setAll(list);
            }

            

            
        });
        
        //transfer
        
        transferId.setCellValueFactory(new PropertyValueFactory<TransferSummary, Integer>("transferId"));
        transferDate.setCellValueFactory(new PropertyValueFactory<TransferSummary, Date>("date"));
        lab.setCellValueFactory(new PropertyValueFactory<TransferSummary, String>("lab"));
        tbl_transferSummary.setItems(transfertable_data);
        loadTransferSummaries();
        
        transItemNo.setCellValueFactory(new PropertyValueFactory<TransferItem, Integer>("itemNo"));
        transDescription.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<TransferItem, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<TransferItem, String> param) {
                return new SimpleObjectProperty<>(param.getValue().getProduct().getDescription());
            }
        });
        transQty.setCellValueFactory(new PropertyValueFactory<TransferItem, Double>("quantity"));
        tbl_transfers.setItems(transferItemtable_data);
        
        tbl_transferSummary.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TransferSummary>() {

            @Override
            public void changed(ObservableValue<? extends TransferSummary> observable, TransferSummary oldValue, TransferSummary newValue) {
                txt_transferId.setText(String.valueOf(newValue.getTransferId()));
                txt_transferDate.setText(String.valueOf(newValue.getDate()));
                txt_recLab.setText(newValue.getLab());
                txt_noOfItems.setText(String.valueOf(newValue.getNoOfItems()));
                transferDetails.setExpanded(true);
                
                Collection<TransferItem> list = new ArrayList<>();
                list.addAll(newValue.getTransferItems());
                transferItemtable_data.setAll(list);
            }
        });
        
        
        
        
        
    }    

    private void loadGRNSummaries() {
        
        Set<GRNSummary> list = new HashSet<>( Hib.start().createCriteria(model.GRNSummary.class).list());
        grntable_data.setAll(list);
        Hib.stop();
    }

    private void loadTransferSummaries() {
        Set<TransferSummary> list = new HashSet<>( Hib.start().createCriteria(model.TransferSummary.class).list());
        
        transfertable_data.setAll(list);
        Hib.stop();
    }
    
}
