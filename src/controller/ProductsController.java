package controller;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;
import model.BioStock;
import model.ChemStock;
import model.Hib;
import model.MainStock;
import model.PhyStock;

import model.Product;
import org.hibernate.Session;

/**
 * FXML Controller class
 *
 * @author Amila
 */
public class ProductsController implements Initializable {

    @FXML
    private TableView<Product> tbl_product;
    @FXML
    private TableColumn<Product, Integer> productId;
    @FXML
    private TableColumn<Product, String> description;
    @FXML
    private TableColumn<Product, String> specification;
    @FXML
    private TableColumn<Product, String> type;
    @FXML
    private TableColumn<Product, String> lab;
    /**
     * Initializes the controller class.
     */
    ObservableList<Product> data = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        productId.setCellValueFactory(new PropertyValueFactory<Product, Integer>("productId"));
        description.setCellValueFactory(new PropertyValueFactory<Product, String>("description"));
        specification.setCellValueFactory(new PropertyValueFactory<Product, String>("specification"));
        type.setCellValueFactory(new PropertyValueFactory<Product, String>("type"));
        lab.setCellValueFactory(new PropertyValueFactory<Product, String>("lab"));

        tbl_product.setItems(data);
        loadProducts();
    }

    @FXML
    private void btn_Add(ActionEvent event) {

        Dialog<Product> addProduct = new Dialog<>();

        addProduct.setTitle("Add new product");
        addProduct.setHeaderText("You can introduce new product to the "
                + "laboratory\nusing this dialog.");
        addProduct.setResizable(false);
        Label lbl_pid = new Label("Product ID: ");
        Label lbl_desc = new Label("Description: ");
        Label lbl_spec = new Label("Specification: ");
        Label lbl_type = new Label("Type: ");
        Label lbl_lab = new Label("Lab: ");

        TextField txt_pid = new TextField(String.valueOf(getNextProductId()));
        txt_pid.setEditable(false);
        TextField txt_desc = new TextField();
        txt_desc.requestFocus();
        txt_desc.setPromptText("Enter the description");
        TextField txt_spec = new TextField();
        txt_spec.setPromptText("Enter the specification");
        TextField txt_type = new TextField();
        txt_type.setPromptText("Enter the type[Chemical,Equipment,Glass Item]");
        TextField txt_lab = new TextField();
        txt_lab.setPromptText("Enter the lab[Biology,Chemistry,Physics]");

        GridPane grid = new GridPane();
        grid.setHgap(30);
        grid.setVgap(10);
        grid.setAlignment(Pos.CENTER_LEFT);

        grid.add(lbl_pid, 1, 1);
        grid.add(txt_pid, 2, 1);
        grid.add(lbl_desc, 1, 2);
        grid.add(txt_desc, 2, 2);
        grid.add(lbl_spec, 1, 3);
        grid.add(txt_spec, 2, 3);
        grid.add(lbl_type, 1, 4);
        grid.add(txt_type, 2, 4);
        grid.add(lbl_lab, 1, 5);
        grid.add(txt_lab, 2, 5);

        addProduct.getDialogPane().setContent(grid);
        ButtonType btn_add = new ButtonType("Add", ButtonData.OK_DONE);
        ButtonType btn_cancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
        addProduct.getDialogPane().getButtonTypes().add(btn_add);
        addProduct.getDialogPane().getButtonTypes().add(btn_cancel);
        addProduct.setResultConverter(new Callback<ButtonType, Product>() {

            @Override
            public Product call(ButtonType param) {
                if (param == btn_add) {
                    return new Product(Integer.valueOf(txt_pid.getText()),
                            txt_desc.getText(),
                            txt_spec.getText(),
                            txt_type.getText(),
                            txt_lab.getText());
                }

                return null;
            }
        });

        Optional<Product> result = addProduct.showAndWait();

        if (result.isPresent()) {
            Session session = Hib.start();
            session.save(result.get());
            MainStock mainStock = new MainStock(result.get(), 0.0);
            BioStock bioStock = new BioStock(result.get(), 0.0);
            ChemStock chemStock = new ChemStock(result.get(), 0.0);
            PhyStock phyStock = new PhyStock(result.get(), 0.0);
            session.save(mainStock);
            session.save(bioStock);
            session.save(phyStock);
            session.save(chemStock);
            Hib.stop();
        }
        loadProducts();

    }

    @FXML
    private void btn_Remove(ActionEvent event) {

        if (tbl_product.getSelectionModel().isEmpty()) {
            new Alert(Alert.AlertType.INFORMATION, "Please select any product to remove!", ButtonType.OK).show();
        } else {
            Hib.start().delete(tbl_product.getSelectionModel().getSelectedItem());
            Hib.stop();
            loadProducts();
        }
    }

    private void loadProducts() {
        List<Product> list = Hib.start().createQuery("FROM Product").list();
        data.clear();
        data.addAll(list);
        Hib.stop();
    }

    private int getNextProductId() {
        try {
            Integer i = (Integer) Hib.start().createQuery("SELECT MAX(productId)+1 FROM Product").uniqueResult();
            return i;
        } catch (NullPointerException e) {
            return 0;
        } finally {
            Hib.stop();
        }

    }

}
