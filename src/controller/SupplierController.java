/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;
import model.Hib;
import model.Product;
import model.Supplier;

/**
 * FXML Controller class
 *
 * @author Amila
 */
public class SupplierController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private TableView<Supplier> tbl_supplier;
    @FXML
    private TableColumn<Supplier, Integer> supplierId;
    @FXML
    private TableColumn<Supplier, String> name;
    @FXML
    private TableColumn<Supplier, String> company;
    @FXML
    private TableColumn<Supplier, String> contactNo;
    
    
    private ObservableList<Supplier> data = FXCollections.observableArrayList();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        supplierId.setCellValueFactory(new PropertyValueFactory<Supplier, Integer>("supplierId"));
        name.setCellValueFactory(new PropertyValueFactory<Supplier, String>("name"));
        company.setCellValueFactory(new PropertyValueFactory<Supplier, String>("company"));
        contactNo.setCellValueFactory(new PropertyValueFactory<Supplier, String>("contactNo"));
        
        tbl_supplier.setItems(data);
        loadSuppliers();
    }    
    
    @FXML
    private void btn_addAction(ActionEvent event){
        Dialog<Supplier> addSupplier = new Dialog<>();

        addSupplier.setTitle("Add new supplier");
        addSupplier.setHeaderText("You can introduce new supplier to the "
                + "laboratory\nusing this dialog.");
        addSupplier.setResizable(false);
        Label lbl_sid = new Label("Supplier ID: ");
        Label lbl_name = new Label("Name: ");
        Label lbl_company = new Label("Company: ");
        Label lbl_contact = new Label("Contact No: ");

        TextField txt_sid = new TextField(String.valueOf(getNexSuppliertId()));
        txt_sid.setEditable(false);
        TextField txt_name = new TextField();
        txt_name.requestFocus();
        txt_name.setPromptText("Enter the supplier name");
        TextField txt_company = new TextField();
        txt_company.setPromptText("Enter the company");
        TextField txt_contact = new TextField();
        txt_contact.setPromptText("Enter the contact no.");

        GridPane grid = new GridPane();
        grid.setHgap(30);
        grid.setVgap(10);
        grid.setAlignment(Pos.CENTER_LEFT);

        grid.add(lbl_sid, 1, 1);
        grid.add(txt_sid, 2, 1);
        grid.add(lbl_name, 1, 2);
        grid.add(txt_name, 2, 2);
        grid.add(lbl_company, 1, 3);
        grid.add(txt_company, 2, 3);
        grid.add(lbl_contact, 1, 4);
        grid.add(txt_contact, 2, 4);

        addSupplier.getDialogPane().setContent(grid);
        ButtonType btn_add = new ButtonType("Add", ButtonBar.ButtonData.OK_DONE);
        ButtonType btn_cancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        addSupplier.getDialogPane().getButtonTypes().add(btn_add);
        addSupplier.getDialogPane().getButtonTypes().add(btn_cancel);
        addSupplier.setResultConverter(new Callback<ButtonType, Supplier>() {

            @Override
            public Supplier call(ButtonType param) {
                if (param == btn_add) {
                    return new Supplier(Integer.valueOf(txt_sid.getText()),
                            txt_name.getText(),
                            txt_company.getText(),
                            txt_contact.getText());
                }

                return null;
            }
        });
        
        Optional<Supplier> result = addSupplier.showAndWait();

        if (result.isPresent()) {
            Hib.start().save(result.get());
            Hib.stop();
        }
        loadSuppliers();
    }
    
    
    @FXML
    private void btn_Remove(ActionEvent event) {

        if (tbl_supplier.getSelectionModel().isEmpty()) {
            new Alert(Alert.AlertType.INFORMATION, "Please select any supplier to remove!", ButtonType.OK).show();
        } else {
            Hib.start().delete(tbl_supplier.getSelectionModel().getSelectedItem());
            Hib.stop();
            loadSuppliers();
        }
    }

    private void loadSuppliers() {
        List<Supplier> list = Hib.start().createQuery("FROM Supplier").list();
        data.clear();
        data.addAll(list);
        Hib.stop();
    }

    private Object getNexSuppliertId() {
        try {
            int i = (Integer) Hib.start().createQuery("SELECT MAX(supplierId)+1 FROM Supplier").uniqueResult();
            return i;
        } catch (NullPointerException e) {
            return 0;
        } finally {
            Hib.stop();
        }
    }
}
