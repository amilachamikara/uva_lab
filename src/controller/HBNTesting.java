/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Hib;
import java.util.List;
import model.Product;
import model.Users;
import net.sf.ehcache.transaction.TransactionTimeoutException;
import org.hibernate.*;

/**
 *
 * @author Amila
 */
public class HBNTesting {

    public static void main(String[] args) {

        addData();
    }

    public static void addData() {
        
        //Product p = new Product(4, "Desc4", "Spec4", "Equip4", "Chem");
//        Users u = new Users(1,"admin","123","admin");
//        Session se = Hib.start();
//        se.save(u);
//        Hib.stop();

    }

    public static void retrieveData()throws TransactionTimeoutException {
        
        Session se = Hib.start();
        try{
        Query q = se.createQuery("FROM Product");
        List<Product> list = q.list();
        for(Product p:list){
            System.out.println(p.getProductId());
        }
        
        }catch(Exception e){
            System.out.println(e);
        }finally{
        
            Hib.stop();
        }
        
    }
}
